# RNConfig #

This is a test of react-native-config.

### How do I get set up `react-native-config` ? ###

* Add `.env`, `.env.staging`, `.env.production` files (Normaly this files need to be under .gitignore)
* `yarn add react-native-config`
* `react-native link react-native-config`
* Follow extra steps for ios and android - https://github.com/luggit/react-native-config#extra-step-for-android (Create separate schemas for ios staging and production)
* Follow instructions to generate signed apk - http://facebook.github.io/react-native/docs/signed-apk-android.html
* Add to package.json run and build scripts: 
```
  "scripts": {
    ...
    "android-dev": "ENVFILE=.env react-native run-android",
    "android-staging": "ENVFILE=.env.staging react-native run-android",
    "android-production": "ENVFILE=.env.production react-native run-android",
    "ios-staging": "ENVFILE=.env.staging react-native run-ios",
    "build-android-production": "export ENVFILE=.env.production && cd android && ./gradlew assembleRelease && cd ..",
    "build-android-staging": "export ENVFILE=.env.staging && cd android && ./gradlew assembleRelease && cd .."
  }
```

### How to use `react-native-config` ###

* iOS: select one of build schemas
* Android: run one of the build or run scripts (`yarn android-staging` ...)
